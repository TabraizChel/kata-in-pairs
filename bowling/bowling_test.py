import unittest
from fizzbuzz import fizz_buzz


class TestFizzBuzz(unittest.TestCase):
    def test_3_is_fizz(self):
        self.assertEqual(fizz_buzz(3), 'Fizz')

    

# This is required to actually run the unit tests
if __name__ == '__main__':
    unittest.main()
