import random
import time
class frame():

    def __init__(self, pins = 10, rolls = 2,frame_score = 0):

        self.pins = pins
        self.rolls = rolls
        self.roll_type = None
        self.frame_score = frame_score


    def roll(self):
        pins_hit = random.randint(0,self.pins)
        self.pins -= pins_hit
        self.rolls -= 1

    def pin_check(self):
        return self.pins

    def rolls_check(self):
        if self.rolls == 0:
            return True

    def get_score(self):
        self.frame_score = 10 - self.pin_check()
        return self.frame_score







class bowling():

    def __init__(self):
        self.rounds = []
        self.total_score = 0
        self.play_game()





    def play_game(self):
        while len(self.rounds) < 10:
            current_frame = frame()
            while not current_frame.rolls_check():
                current_frame.roll()
                if self.strike_type(current_frame):
                    time.sleep(5)
                    break
            print('get ready for next frame !!!!!!! \n')
            time.sleep(5)


        self.score()
        print('Good game, you got a score of: ' ,self.total_score ,' !!!')



    def score(self):
        for frame in range(len(self.rounds)):
            if self.rounds[frame].roll_type == 'strike' and len(self.rounds) - frame > 2:
                self.total_score = self.total_score + self.rounds[frame].get_score() + self.rounds[frame + 1].get_score() + self.rounds[frame + 2].get_score()

            elif self.rounds[frame].roll_type == 'spare' or self.rounds[frame].roll_type == 'strike' and len(self.rounds) - frame > 1:
                self.total_score = self.total_score + self.rounds[frame].get_score() + self.rounds[frame + 1].get_score()

            else:
                self.total_score += self.rounds[frame].get_score()

    def strike_type(self, current_frame):
        pins_left = []
        if current_frame.pin_check() == 0:
            print('strike !!!!!!!!')
            current_frame.roll_type = 'strike \n'
            self.rounds.append(current_frame)
            pins_left.append(current_frame.pin_check())
            #time.sleep(5)
            return True
        else:
            if current_frame.pin_check() == 0:
                print('Spare !!!!!!!!')
                current_frame.roll_type = 'spare \n'
                self.rounds.append(current_frame)
                pins_left.append(current_frame.pin_check())
                #time.sleep(5)
                return True
        if current_frame.rolls_check():
            self.rounds.append(current_frame)
            pins_left.append(current_frame.pin_check())
            current_frame.roll_type = 'normal'
            print(current_frame.pin_check(),' pins left in this frame')

        else:
            print(current_frame.pin_check() ,'pins left get ready for next round !! \n')




if __name__ == '__main__':

    test = bowling()
