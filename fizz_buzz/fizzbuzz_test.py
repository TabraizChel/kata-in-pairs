import unittest
from fizzbuzz import fizz_buzz


class TestFizzBuzz(unittest.TestCase):
    def test_3_is_fizz(self):
        self.assertEqual(fizz_buzz(3), 'Fizz')

    def test_9_is_fizz(self):
        self.assertEqual(fizz_buzz(9), 'Fizz')

    def test_123_is_fizz(self):
        self.assertEqual(fizz_buzz(123), 'Fizz')

    def test_5_is_buzz(self):
        self.assertEqual(fizz_buzz(5), 'Buzz')

    def test_20_is_buzz(self):
        self.assertEqual(fizz_buzz(20), 'Buzz')

    def test_200_is_buzz(self):
        self.assertEqual(fizz_buzz(200), 'Buzz')

    def test_15_is_fizzbuzz(self):
        self.assertEqual(fizz_buzz(15), 'FizzBuzz')

    def test_45_is_fizzbuzz(self):
        self.assertEqual(fizz_buzz(45), 'FizzBuzz')

    def test_7_is_pop(self):
        self.assertEqual(fizz_buzz(7), 'Pop')

    def test_28_is_pop(self):
        self.assertEqual(fizz_buzz(28), 'Pop')

    def test_77_is_pop(self):
        self.assertEqual(fizz_buzz(77), 'Pop')

    def test_1_is_1(self):
        self.assertEqual(fizz_buzz(1), 1)

    def test_2_is_2(self):
        self.assertEqual(fizz_buzz(2), 2)

    def test_6_is_fizz(self):
        self.assertEqual(fizz_buzz(6), 'Fizz')

    def test_10_is_buzz(self):
        self.assertEqual(fizz_buzz(10), 'Buzz')

    def test_30_is_fizzbuzz(self):
        self.assertEqual(fizz_buzz(30), 'FizzBuzz')

    def test_21_is_fizzPop(self):
        self.assertEqual(fizz_buzz(21), 'FizzPop')

    def test_63_is_fizzPop(self):
        self.assertEqual(fizz_buzz(63), 'FizzPop')

    def test_126_is_fizzPop(self):
        self.assertEqual(fizz_buzz(126), 'FizzPop')

    def test_35_is_buzzpop(self):
        self.assertEqual(fizz_buzz(35), 'BuzzPop')

    def test_70_is_buzzpop(self):
        self.assertEqual(fizz_buzz(70), 'BuzzPop')

    def test_140_is_buzzpop(self):
        self.assertEqual(fizz_buzz(140), 'BuzzPop')

    def test_105_is_fizzbuzzpop(self):
        self.assertEqual(fizz_buzz(105), 'FizzBuzzPop')

    def test_210_is_fizzbuzzpop(self):
        self.assertEqual(fizz_buzz(210), 'FizzBuzzPop')

    def test_315_is_fizzbuzzpop(self):
        self.assertEqual(fizz_buzz(315), 'FizzBuzzPop')




# This is required to actually run the unit tests
if __name__ == '__main__':
    unittest.main()
