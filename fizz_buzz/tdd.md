# An introduction to TDD (with FizzBuzz)

First, let's get some misconceptions out of the way. There are many, so let's address some of the most common ones: 

* **TDD means you have to write all the tests before you begin coding.** In fact, you should only be writing *one* test before you start coding. 

* **TDD is much more work.** Whether you and your team follow TDD or not, your software and your code should be tested. TDD simply provides a workflow that means you spend less time ensuring test coverage and creating 'bolt-on' tests. 

----------------

TDD  workflow is made up of the repetition of three steps: 

1. **Test Fails:** Write a test that fails.

2. **Test Passes:** Make a change to the code to  make the test pass.

3. **Refactor:** Refactor the code. 

This may seem somewhat counterintutive. Why would you write a test you know will fail? Let's dive into an example to demonstrate how this approach works.

### FizzBuzz

Many of you will have already heard about FizzBuzz. It's a simple algorithm, but one that works very well as an introduction to TDD. FizzBuzz works by translating the input of a number in accordance with some simple rules:

1. If the number is **divisible by 3** we should return **Fizz**.
2. If the number is **divisible by 5** we should return **Buzz**.
3. If the number is **divisible by 15** we should return **FizzBuzz**.
4. If none of the above rules apply, we should return the number itself.

There are lots of ways to approach and implement this, but this exercise is designed to demonstrate the use of a TDD workflow in solving the problem 9rather than arriving at and implementing the solution as quickly as possible). 

Let's work through the problem using TDD. 

### 1. Write a test that fails

It may feel counterintuitive to write a test you know will fail, but this is the first and most important step in the practice of TDD. 

Let's write our first test in a file called `fizzbuzz_test.py`

```python
import unittest

class TestFizzBuzz(unittest.TestCase):
    def test_3_is_fizz(self):
        self.assertEqual(fizz_buzz(3), 'Fizz')

# This is required to actually run the unit tests
if __name__ == '__main__':
	unittest.main()
```

Let's now run the test to confirm that we have a failure. The truncated output should look something like: 

```
$ python fizzbuzz_test.py 
E
----------------------------------------------------------------------

Ran 1 test in 0.000s

FAILED (errors=1)
```

The failure in this case is a compilation error, since the `fizz_buzz` function does not exist yet (as we haven't actually written any code).

It's perhaps worth noting that in python each unit test run is represented by a character. A `.` would represent a success. In this case we have an `E` which represents an error (unable to compile), but we could also have an `F`, which represents a failure (that the result is not what was expected).

### 2. Make a change so that the test passes

Now that we have our failing test, we can resolve this. We want to write some code to fix our test. We should make the smallest possible change to achieve this (remember that we want our subsequent test to fail, so we should not make any assumptions about what we will want our code to do in the future).

The simplest solution is to create a `fizz_buzz` function that returns "Fizz". We will do this is a new file called `fizzbuzz.py`

```python
def fizz_buzz(number): 
    return 'Fizz'

```

If we run our tests with the above code added, it will still fail. We need to import our new `fizz_buzz` method into our test file: 

```python
import unittest
from fizzbuzz import fizz_buzz

class TestFizzBuzz(unittest.TestCase):
    def test_3_is_fizz(self):
        self.assertEqual(fizz_buzz(3), 'Fizz')

# This is required to actually run the unit tests
if __name__ == '__main__':
	unittest.main()
```

Running our tests again we should get our first passing test:

```
 $ python fizzbuzz_test.py                
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
```

### 3. Refactor

For this first round of TDD there is no possible way to make the current code cleaner. It's time to start thinking about the next test...

-----------------------------------

### 1. Write a test that fails

```python
 def test_5_is_buzz(self):
      self.assertEqual(fizz_buzz(5), 'Buzz')
```

Run the tests to confirm the failure before you continue onto the next step. 

### 2. Make a change so that the test passes

```python
def fizz_buzz(number): 
 if number == 5:
        return 'Buzz'
    return 'Fizz'
```

### 3. Refactor

Once again, there is nothing we can clean up here. Time to move on to the next test...

------------------------------------

### 1. Write a test that fails

```python
 def test_15_is_fizzbuzz(self):
        self.assertEqual(fizz_buzz(15), 'FizzBuzz')
```

### 2. Make a change so that the test passes

```python
def fizz_buzz(number):
    if number == 15: 
        return 'FizzBuzz'
    if number == 5:
        return 'Buzz'
    return 'Fizz'
```

### 3. Refactor

There's still  no need for any refactor. On to the next...

------------------------------------

### 1. Write a test that fails

```python
    def test_1_is_1(self):
        self.assertEqual(fizz_buzz(1), 1)
```

### 2. Make a change so that the test passes

```python
def fizz_buzz(number):
    if number == 15:
        return 'FizzBuzz'
    if number == 5:
        return 'Buzz'
    if number == 1:
        return 1
    return 'Fizz'

```

### 3. Refactor

Still nothing...

------------------------------

### 1. Write a test that fails

```python
    def test_2_is_2(self):
        self.assertEqual(fizz_buzz(2), 2)
```

### 2. Make a change so that the test passes

```python
    def fizz_buzz(number):
    if number == 15:
        return 'FizzBuzz'
    if number == 5:
        return 'Buzz'
    if number == 1 or number == 2:
        return number
    return 'Fizz'
```

### 3. Refactor

There's room for these statements to be simplified, so let's carry out our first refactor:

```python
def fizz_buzz(number):
    if number == 15:
        return 'FizzBuzz'
    if number == 5:
        return 'Buzz'
    if number == 3:
        return 'Fizz'
    return number
```

Now we have made another change to the code through our refactor, so it's important for us to run the tests again to ensure everything is still passing before we move on to the next test case.

---------------------------

### 1. Write a test that fails

```python
    def test_6_is_fizz(self):
        self.assertEqual(fizz_buzz(6), 'Fizz')
```

### 2. Make a change so that the test passes

```python
def fizz_buzz(number):
    if number == 15:
        return 'FizzBuzz'
    if number == 5:
        return 'Buzz'
    if number % 3 == 0:
        return 'Fizz'
    return number
```

### 3. Refactor

There's nothing to refactor at this time

--------------------

### 1. Write a test that fails

```python
 def test_10_is_buzz(self):
        self.assertEqual(fizz_buzz(10), 'Buzz')
```

### 2. Make a change so that the test passes

```python
def fizz_buzz(number):
    if number == 15:
        return 'FizzBuzz'
    if number % 5 == 0:
        return 'Buzz'
    if number % 3 == 0:
        return 'Fizz'
    return number
```

### Refactor

Still nothing. 

--------------------

### 1. Write a test that fails

```python
    def test_30_is_fizzbuzz(self):
        self.assertEqual(fizz_buzz(30), 'FizzBuzz’)
```

### 2. Make a change so that the test passes

```python
def fizz_buzz(number):
    if number % 15 == 0:
        return 'FizzBuzz'
    if number % 5 == 0:
        return 'Buzz'
    if number % 3 == 0:
        return 'Fizz'
    return number
```

### 3. Refactor

At this point, it would be possible to write the code to take advantage of the fact 5 and 3 are factors of 15, but the result code would actually be more complicated than the current solution. So we won't actually do that.

--------------------

### 1. Write a test that fails


There is no further test that can be written that can break the rules of the original requirements. You may wish to introduce further refactors, and you can because you now have tests covering all of the requirements, so if your refactor introduces a breaking change to your code, you will be told about it when you run your tests. 

For your reference, the code for this walkthough is provided [here](./fizz_buzz)







