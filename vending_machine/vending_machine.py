import time
class vending_machine(object):
    def __init__(self,current_amount = 0, run = None):

        def generate_product_key(products):
            product_int = {}
            i = 0
            for key in products.keys():
                product_int[i] = key
                i += 1
            return product_int

        def generate_product_stock(products):
            product_stock = {}
            for key in products.keys():
                product_stock[key] = 1
            return product_stock


        valid_coins = {'5p':0.05,'10p': 0.1,'20p':0.2 , '50p':0.5 , '£1':1,'£2':2}
        products = {'cola': 1, 'crisps': 0.5, 'chocolate':0.5}
        product_stock = generate_product_stock(products)
        product_key = generate_product_key(products)
        self.products = products
        self.valid_coins = valid_coins
        self.rejected_coins = { 'foreign_currency':0, 'credit receipt' : 0}
        self.current_amount = current_amount
        self.product_key = product_key
        self.product_stock = product_stock

        if run != None:
            self.lifetime()

    def lifetime(self):
        while True:

            self.display_money()

            for i in range(int(input('how many coins would you like to insert ?? (int): '))):
                self.add_money()

            if self.return_money():
                continue

            print('select item: ','   ', self.product_key)
            input_key = int(input())
            product = self.select_product(input_key)
            product_amount = self.products[product]



            if self.stock_check(product):
                if self.money_check(input_key):
                    print('You got: ', product, '!!!')
                    self.product_stock[product] -= 1
                    self.rejected_coins['credit receipt'] += self.current_amount - product_amount
                    self.current_amount = 0

                else:
                    print('need  ' , product_amount - self.current_amount, '  extra for purchase')

            else:
                print('Product out of stock, returning money... ')
                continue




            time.sleep(3)





    def return_money(self):
        x = input('press 0 to return money or press any other key to continue')
        if x == '0':
            self.rejected_coins['credit receipt'] += self.current_amount
            self.current_amount = 0
            return True



    def display_money(self):
        print('Current amount: ',('£ {0}').format(self.current_amount),'    Returned coins: ', self.rejected_coins )


    def add_money(self):
        if self.current_amount == 0:
            coin = input('please insert coins (UK ONLY, 5p 10p 20p 50p £1 £2 accepted): ')
            self.accept_coin(coin)
            self.display_money()
            if self.rejected_coins['credit receipt'] != 0:
                self.current_amount += self.rejected_coins['credit receipt']
                self.display_money()

        else:
            input_money = input('input more money (UK ONLY, 5p 10p 20p 50p £1 £2 accepted): ')
            self.accept_coin(input_money)
            self.display_money()



    def set_current_amount(self, new_amount):
        self.current_amount = new_amount


    def stock_check(self,product):
        if self.product_stock[product] > 0:

            return True



    def select_product(self, int_key):
        return self.product_key[int_key]


    def money_check(self,product_int):
        if self.current_amount >= self.products[self.product_key[product_int]]:
            return True


    def accept_coin(self,coin):
        if coin in self.valid_coins:
            new_amount = self.current_amount + self.valid_coins[coin]
            self.set_current_amount(new_amount)
            return True
        else:
            print('not valid currency')

            self.rejected_coins['foreign_currency'] += 1
            return False





if __name__ == '__main__':
  US_vending = vending_machine(run = True)
