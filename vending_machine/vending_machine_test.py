import unittest
from vending_machine import vending_machine

class TestVending(unittest.TestCase):
    def test_valid_currency(self):
        test_machine = vending_machine()
        self.assertTrue(test_machine.accept_coin('dime'))

    def test_invalid_currency(self):
        test_machine = vending_machine()
        self.assertFalse(test_machine.accept_coin('penny'))

    def test_money_check(self):
        test_machine = vending_machine()
        self.assertFalse(test_machine.money_check(0))






# This is required to actually run the unit tests
if __name__ == '__main__':
    unittest.main()
